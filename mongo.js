// db.users.insertOne({
// 
//     "username": "dahyunTwice",
//     "password": "dahyunKim"
// 
// })

// db.users.insertOne({
// 
//     "username": "gokuSon",
//     "password": "over9000"
// 
// })
// db.users.insertMany(
//     [
//         {
//             "username": "pablo123",
//             "password": "123paul"
//         },
//         {
//             "username": "pedro99",
//             "password": "iampeter99"
//         }
//     ]
// )
// db.products.insertMany([
//     {
//         "name": "sword",
//         "description": "Long Sword",
//         "price": 10000
//      },
//      {
//         "name": "shield",
//         "description": "Angel's Plate Shield",
//         "price": 20000
//      },
//      {
//         "name": "armor",
//         "description": "Breast Plate",
//         "price": 5000
//      }
// 
// ])

//lets you find the collection in the database
// db.users.find()

//db.users.find({"username": "pedro99"})

// db.cars.insertMany([
// 
//     {
//         "name": "Vios",
//         "brand": "Toyota",
//         "type": "sedan",
//         "price": 1500000
//     },
//     {
//         "name": "Tamaraw FX",
//         "brand": "Toyota",
//         "type": "auv",
//         "price": 75000
//     },
//     {
//         "name": "City",
//         "brand": "Honda",
//         "type": "sedan",
//         "price": 1600000
//     }
// 
// ])
//db.cars.find({"type": "sedan"})
//db.cars.find({"brand": "Toyota"})

// it finds the first item dpcument in the collections
//db.cars.findOne({})

// it finds only one item that has the same criteria
//db.cars.findOne({"type": "sedan"})


// Update
//db.users.updateOne({"username": "pedro99"},{$set:{"username":"peter1999"}})
//it allows you to change the first item specific field
//db.users.updateOne({},{$set:{"username": "updatedUsername"}})
// allows you to add a field that is not existing
//db.users.updateOne({"username":"pablo123"},{$set:{"isAdmin": true}})
// allow us to update all documents and can add a field
//db.users.updateMany({},{$set:{"isAdmin": true}})
//with criteria, it will update all item the has the same criteria
//db.cars.updateMany({"type": "sedan"},{$set:{"price": 1000000}})

// Delete
// deletes the first item
//db.products.deleteOne({})
// deletes the first item that matched the criteria
//db.cars.deleteOne({"brand": "Toyota"})
// deletes all that matches the criteria
db.users.deleteMany({"isAdmin": true})


